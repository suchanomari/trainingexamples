# TrainingExamples



## What it contains.
- Inheritance Example.
- Polymorphism Example.
- Override vs Overload Example.
- String Class Methods Showcase.
- Object Class Showcase.
~> Encapsulation was applied to all examples.

## Description
This is a repository made for containing the java training examples that were requested on the 9th of August.
