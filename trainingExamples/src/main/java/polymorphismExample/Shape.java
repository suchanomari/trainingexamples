package polymorphismExample;

public interface Shape {
	String getShapeName();
}
