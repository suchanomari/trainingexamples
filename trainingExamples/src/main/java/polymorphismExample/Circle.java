package polymorphismExample;

public class Circle implements Shape {
	public Circle() {
	}

	@Override
	public String getShapeName() {
		return "This shape is a circle";
	}
}
