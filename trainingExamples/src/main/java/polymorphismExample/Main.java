package polymorphismExample;

public class Main {

	public static void main(String[] args) {
		/**
		 * Polymorphism Example: this example illustrate the polymorphism concept.
		 * [@Override is used to override the interface method]
		 */
		Circle circle = new Circle(); // object from Circle class [implements Shape interface]
		Triangle triangle = new Triangle(); // object from Triangle class [implements Shape interface]
		// Note that the Shape interface which both implements, enforces them to
		// override the method getShapeName
		// now we can refer to both circle and triangle objects with "Shape" interface
		// instead of their classes.
		// to illustrate, they both can use the "whatIsThisShape" method as shown below.
		System.out.println(whatIsThisShape(circle));
		System.out.println(whatIsThisShape(triangle));
		/**
		 * This allowed us to use one method "whatIsThisShape(Shape shape) instead of
		 * two: whatIsThisShape(Circle shape) and whatIsThisShape(Triangle shape)
		 * [reduced redundancy] and this will allow any future class that implements the
		 * Shape interface to use this method as well.
		 */

		// can also be used to change an object class easily.
		Shape newShape = new Circle(); // shape of circle class.

		System.out.println(whatIsThisShape(newShape)); // to test that its currently of Circle class

		newShape = new Triangle(); // now its of triangle class.

		System.out.println(whatIsThisShape(newShape)); // to test that its currently of Triangle class
	}

	public static String whatIsThisShape(Shape shape) {
		return shape.getShapeName();
	}
}
