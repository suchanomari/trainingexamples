package polymorphismExample;

public class Triangle implements Shape {

	public Triangle() {
	}

	@Override
	public String getShapeName() {
		return "This Shape is a triangle";
	}

}
