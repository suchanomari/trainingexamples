package StringsExample;

public class Main {

	public static void main(String[] args) {
		/**
		 * this example demonstrate the methods in the String class and many ways to use
		 * it.
		 */
		String testingString = "";
		// to check whether the string is empty or not
		System.out.println("Is the String Empty? " + testingString.isEmpty()); // using isEmpty() method
		System.out.println("Is the String Empty? " + testingString.isBlank()); // using isBlank() method
		System.out.println("Is the String Empty? " + testingString.equals("")); // using equals() method
		// Please note that the string that only contains spaces will be considered
		// blank but it won't be considered empty since length != 0.
		testingString = "  ";
		System.out.println("Is the String Empty? " + testingString.isEmpty()); // using isEmpty() method
		System.out.println("Is the String Blank? " + testingString.isBlank()); // using isBlank() method

		// To find how many does a string contains a certain character
		testingString = "This is a testing string";
		char c = 't';
		int result = 0;
		// Method 1: Looping over all the characters in the string
		result = 0;
		for (int i = 0; i < testingString.length(); i++) {
			if (Character.compare(testingString.charAt(i), c) == 0) { // used CharAt() method
				result++;
			}
		}
		System.out.println("How many '" + c + "' in the testing string? " + result);
		// Method 2: looping over the character array
		result = 0;
		char[] testingCharacters = testingString.toCharArray(); // used toCharArray() method
		for (int i = 0; i < testingCharacters.length; i++) {
			if (testingCharacters[i] == c) {
				result++;
			}
		}
		System.out.println("How many '" + c + "' in the testing string? " + result);

		// to find if two strings are equal (not case sensitive)
		String s1 = "THIS IS STRING";
		String s2 = "this is string";
		// Method 1: equalsIgnoreCase() method
		System.out.println("Are the two equal?" + s1.equalsIgnoreCase(s2));
		// Method 2: using compareToIgnoreCase() method
		System.out.println("Are the two equal?" + (s1.compareToIgnoreCase(s2) == 0));
	}
}
