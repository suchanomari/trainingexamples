package ObjectExample;

public class Main {

	public static void main(String[] args) {
		/**
		 * Object Class Example
		 */
		Student student1 = new Student(1, "Salsabeel");
		Student student2 = new Student(1, "Salsabeel");

		// Testing the toString() method that got overridden.
		System.out.println("toString(): " + student1.toString());

		// testing equal() method
		System.out.println("equal(): " + student1.equals(student2));

		// testing hashCode() method (if two equal objects has same hashCode)
		System.out.println("hashcode(): " + (student1.hashCode() == student2.hashCode()));
	}
}
