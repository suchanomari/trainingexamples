package EncapsulationExample;

public class Main {

	public static void main(String[] args) {
		/**
		 * This example illustrate the Encapsulation concept
		 */
		Rectangle rectangle = new Rectangle(5, 5);

		// all fields are private and can only be accessed through getters and setters
		// (only access points)
		System.out.println("Rectangle current Height: " + rectangle.getHeight());
		System.out.println("Rectangle current Width: " + rectangle.getWidth());
		// Changing values with setters:
		rectangle.setHeight(4);
		rectangle.setWidth(4);
		System.out.println("Rectangle current Height: " + rectangle.getHeight());
		System.out.println("Rectangle current Width: " + rectangle.getWidth());
		// Using public methods:
		System.out.println("Rectangle Area: " + rectangle.calculateArea());

	}

}
