package overrideVSOverload;

public class Main {

	public static void main(String[] args) {
		/**
		 * Override Example: there's two classes (Student, User) where (Student) class
		 * inherit the (User) class, making all methods in it available, however, one
		 * method (getExecutedClass) is overridden in (Student) class which means that
		 * the (getExecutedClass) method that is in (Student) will be the one get
		 * executed not the one in (User) class. As shown below.
		 */
		Student student = new Student(); // object of class student
		System.out.println(student.getExecutedClass()); // to show it used the method in Student class not in User
														// class.
		/**
		 * Overload Example: made two methods in Student which are both called (setName)
		 * but one takes two parameters (first name, last name) while the other one
		 * takes three (first, middle, last names), we can choose which of the two to
		 * call by providing the correct number of parameters.
		 */
		student.setName("myFirstName", "myLastName");
		System.out.println("Student Name is: " + student.getName()); // to get the current name
		student.setName("myFirstName", "myMiddleName", "myLastName");
		System.out.println("Student Name is: " + student.getName()); // to get the current name
	}

}
