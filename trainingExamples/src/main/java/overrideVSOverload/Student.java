package overrideVSOverload;

public class Student extends User {
	private String firstName;
	private String middleName;
	private String lastName;

	public Student() {
	}

	public void setName(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public void setName(String firstName, String middleName, String lastName) {
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
	}

	public String getName() {
		String studentName;
		if (this.middleName == null) {
			studentName = this.firstName + " " + this.lastName;
		} else {
			studentName = this.firstName + " " + this.middleName + " " + this.lastName;
		}
		return studentName;
	}

	@Override
	public String getExecutedClass() {
		return "Executed the Method in Student Class";
	}
}
