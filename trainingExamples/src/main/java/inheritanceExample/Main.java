package inheritanceExample;

public class Main {

	public static void main(String[] args) {
		/**
		 * Inheritance Example.
		 */
		FullTimeEmp fullTimeEmployee = new FullTimeEmp(1, "employee1", 500); // Full time employee object [has inherited Employee Class]
		PartTimeEmp partTimeEmployee = new PartTimeEmp(2, "partTimer1"); // part time employee object [has inherited Employee Class]
		
		// Testing to see if it inherited methods that were in Employee class:
		System.out.println("The employee id is: " + fullTimeEmployee.getEmpID());
		System.out.println("The employee name is: "+ fullTimeEmployee.getEmpName());
		
		//can also use methods inside its own class
		System.out.println("The Employee Salary is: "+ fullTimeEmployee.getSalary());
		
		//part time employee can also access methods inside the employee class that it inherited.
		System.out.println("The Employee id is: "+partTimeEmployee.getEmpID());
		System.out.println("The Employee Name is: "+partTimeEmployee.getEmpName());
		
		//but part time employee cannot access methods inside the full time employee class.
		//not valid: partTimeEmployee.getSalary();
	}
}
