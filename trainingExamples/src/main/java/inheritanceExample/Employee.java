package inheritanceExample;

public class Employee {
	private int empID;
	private String empName;
	public Employee(int empID, String empName) {
		this.empID = empID;
		this.empName = empName;
	}
	public int getEmpID() {
		return empID;
	}
	public String getEmpName() {
		return empName;
	}
}
