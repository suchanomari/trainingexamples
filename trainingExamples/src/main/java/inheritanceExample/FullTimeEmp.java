package inheritanceExample;

public class FullTimeEmp extends Employee {
	private int salary;

	public FullTimeEmp(int empID, String empName, int salary) {
		super(empID, empName);
		this.salary = salary;
	}

	public int getSalary() {
		return salary;
	}
	
}
